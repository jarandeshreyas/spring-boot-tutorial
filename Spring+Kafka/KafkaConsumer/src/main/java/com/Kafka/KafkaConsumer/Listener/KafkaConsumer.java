package com.Kafka.KafkaConsumer.Listener;

import com.Kafka.KafkaConsumer.Model.User;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

    @KafkaListener(topics = "Kafka_Example", groupId = "group_id")
    public void consume(String message){

        System.out.println("Consumed message "+message);

    }

    @KafkaListener(topics = "Kafka_Example_Json", groupId = "group_id", containerFactory = "userKafkaListenerFactory")
    public void consumeJson(User user){
        System.out.println("Message JSON message "+user);
    }


}
