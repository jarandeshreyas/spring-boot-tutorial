package com.DesignPattern.State;

public interface CandyVendingMachineState {

    void insertCoin();
    void dispense();
    void pressButton();

}
