package com.DesignPattern.Builder;

import org.junit.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuilderApplication {

	@Test
	public static void main(String[] args) throws Exception {
		SpringApplication.run(BuilderApplication.class, args);



			HouseBuilder concreteHouse = new ConcreteHouse();
			ConstructionEngineer engineerA = new ConstructionEngineer(concreteHouse);
			House houseA = engineerA.constructHouse();
			System.out.println("House is: "+houseA);
			PrefabricatedHouse prefabricatedHouse = new PrefabricatedHouse();
			ConstructionEngineer engineerB = new ConstructionEngineer(prefabricatedHouse);
			House houseB = engineerB.constructHouse();
			System.out.println("House is: "+houseB);


	}

}
