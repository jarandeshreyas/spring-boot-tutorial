package com.DesignPattern.Builder;

public class PrefabricatedHouse implements HouseBuilder {

    private House house;
    public PrefabricatedHouse() {
        this.house = new House();
    }
    @Override
    public void buildFoundation() {
        house.setFoundation("Wood, laminate, and PVC flooring");
        System.out.println("PrefabricatedHouse: Foundation complete...");
    }
    @Override
    public void buildStructure(){
        house.setStructure("Structural steels and wooden wall panels");
        System.out.println("PrefabricatedHouse: Structure complete...");
    }
    @Override
    public void buildRoof(){
        house.setRoof("Roofing sheets");
        System.out.println("PrefabricatedHouse: Roof complete...");
    }
    @Override
    public void paintHouse(){
        house.setPainted(false);
        System.out.println("PrefabricatedHouse: Painting not required...");
    }
    @Override
    public void furnishHouse(){
        house.setFurnished(true);
        System.out.println("PrefabricatedHouse: Furnishing complete...");
    }
    public House getHouse() {
        System.out.println("PrefabricatedHouse: Prefabricated house complete...");
        return this.house;
    }

}
