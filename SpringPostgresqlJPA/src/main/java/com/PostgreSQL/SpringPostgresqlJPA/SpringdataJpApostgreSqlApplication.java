package com.PostgreSQL.SpringPostgresqlJPA;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@SpringBootApplication
public class SpringdataJpApostgreSqlApplication implements CommandLineRunner {

	@Autowired
	private StudentRepository studentRepo;

	public static void main(String[] args) {
		SpringApplication.run(SpringdataJpApostgreSqlApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		List<Student> students =studentRepo.findAll();
		students.forEach(System.out::println);

		System.exit(0);


	}
}
