package com.DesignPattern.Abstract.PizzaConfig;

import com.DesignPattern.Abstract.ToppingFactoryConfig.BaseToppingFactory;

public class CheesePizza extends Pizza{

    BaseToppingFactory toppingFactory;

    public CheesePizza(BaseToppingFactory toppingFactory) {
        this.toppingFactory = toppingFactory;
    }

    @Override
    public void addIngredients() {

        System.out.println("Preparing ingredients for cheese pizza... ");
        toppingFactory.createCheese();
        toppingFactory.createSauce();

    }
}
