package com.DesignPattern.Abstract.PizzaConfig;

import com.DesignPattern.Abstract.ToppingFactoryConfig.BaseToppingFactory;

public class VeggiePizza extends Pizza {

    BaseToppingFactory toppingFactory;

    public VeggiePizza(BaseToppingFactory toppingFactory) {
        this.toppingFactory = toppingFactory;
    }

    @Override
    public void addIngredients() {
        System.out.println("Preparing ingredients for Veggie pizza.... ");
        toppingFactory.createCheese();
        toppingFactory.createSauce();

    }
}
