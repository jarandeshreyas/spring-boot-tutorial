package com.DesignPattern.Abstract;

import com.DesignPattern.Abstract.BaseConfig.BasePizzaFactory;
import com.DesignPattern.Abstract.BaseConfig.GourmetPizzaFactory;
import com.DesignPattern.Abstract.BaseConfig.SicilianPizzaFactory;
import com.DesignPattern.Abstract.PizzaConfig.Pizza;
import org.junit.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AbstractApplication {

	@Test
	public static void main(String[] args) throws Exception {
		SpringApplication.run(AbstractApplication.class, args);

		System.out.println("Testing Gourmet Pizza Factory....");
		BasePizzaFactory pizzaFactory1=new GourmetPizzaFactory();
		Pizza cheesePizza1= pizzaFactory1.createPizza("cheese");
		Pizza veggiePizza=pizzaFactory1.createPizza("veggie");


		System.out.println("Testing Sicilian Pizza Factory....");
		BasePizzaFactory pizzaFactory2=new SicilianPizzaFactory();
		Pizza cheesePizza2=pizzaFactory2.createPizza("cheese");
		Pizza pepperoniPizza =pizzaFactory2.createPizza("pepperoni");

	}

}
