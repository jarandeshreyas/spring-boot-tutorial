package com.DesignPattern.Abstract.BaseConfig;

import com.DesignPattern.Abstract.PizzaConfig.CheesePizza;
import com.DesignPattern.Abstract.PizzaConfig.PepperoniPizza;
import com.DesignPattern.Abstract.PizzaConfig.Pizza;
import com.DesignPattern.Abstract.PizzaConfig.VeggiePizza;
import com.DesignPattern.Abstract.ToppingFactoryConfig.BaseToppingFactory;
import com.DesignPattern.Abstract.ToppingFactoryConfig.SicilianToppingFactory;

public class SicilianPizzaFactory extends BasePizzaFactory {
    @Override
    public Pizza createPizza(String type) {
        Pizza pizza;
        BaseToppingFactory toppingFactory= new SicilianToppingFactory();
        pizza = switch (type.toLowerCase()) {
            case "cheese" -> new CheesePizza(toppingFactory);
            case "pepperoni" -> new PepperoniPizza(toppingFactory);
            case "veggie" -> new VeggiePizza(toppingFactory);
            default -> throw new IllegalArgumentException("No such pizza.");
        };
        pizza.addIngredients();
        pizza.bakePizza();
        return pizza;
    }
}
