package com.DesignPattern.Abstract.BaseConfig;

import com.DesignPattern.Abstract.PizzaConfig.Pizza;

public abstract class BasePizzaFactory {

    public abstract Pizza createPizza(String type);

}
