package com.DesignPattern.Abstract.CheeseConfig;

public interface Cheese {

    void prepareCheese();

}
