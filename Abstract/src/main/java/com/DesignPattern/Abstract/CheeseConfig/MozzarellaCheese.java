package com.DesignPattern.Abstract.CheeseConfig;

public class MozzarellaCheese implements Cheese {

    public MozzarellaCheese(){
        prepareCheese();
    }

    @Override
    public void prepareCheese() {
        System.out.println("Preparing Mozzarella cheese.... ");
    }
}
