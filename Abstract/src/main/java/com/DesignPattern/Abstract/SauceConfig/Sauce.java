package com.DesignPattern.Abstract.SauceConfig;

public interface Sauce {

    void prepareSauce();

}
