package com.DesignPattern.Abstract.ToppingFactoryConfig;

import com.DesignPattern.Abstract.CheeseConfig.Cheese;
import com.DesignPattern.Abstract.SauceConfig.Sauce;

public abstract class BaseToppingFactory {

    public abstract Cheese createCheese();
    public abstract Sauce createSauce();

}
