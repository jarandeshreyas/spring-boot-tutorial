package com.DesignPattern.Abstract.ToppingFactoryConfig;

import com.DesignPattern.Abstract.CheeseConfig.Cheese;
import com.DesignPattern.Abstract.CheeseConfig.MozzarellaCheese;
import com.DesignPattern.Abstract.SauceConfig.Sauce;
import com.DesignPattern.Abstract.SauceConfig.TomatoSauce;

public class SicilianToppingFactory extends BaseToppingFactory {
    @Override
    public Cheese createCheese() {
        return new MozzarellaCheese();
    }

    @Override
    public Sauce createSauce() {
        return new TomatoSauce();
    }
}
