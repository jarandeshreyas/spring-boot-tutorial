package com.DesignPattern.Abstract.ToppingFactoryConfig;

import com.DesignPattern.Abstract.CheeseConfig.Cheese;
import com.DesignPattern.Abstract.CheeseConfig.GoatCheese;
import com.DesignPattern.Abstract.SauceConfig.CaliforniaOilSauce;
import com.DesignPattern.Abstract.SauceConfig.Sauce;

public class GourmetToppingFactory extends BaseToppingFactory {
    @Override
    public Cheese createCheese() {
        return new GoatCheese();
    }

    @Override
    public Sauce createSauce() {
        return new CaliforniaOilSauce();
    }
}
