package com.DesignPattern.FactoryPattern.factory.impl;

import com.DesignPattern.FactoryPattern.factory.OperatingSystem;
import org.springframework.stereotype.Component;

@Component
public class Ios implements OperatingSystem {

    @Override
    public String specification(){

       return "IOS : The most Secured OS ";

    }

}
