package com.DesignPattern.FactoryPattern.factory.impl;

import com.DesignPattern.FactoryPattern.factory.OperatingSystem;
import org.springframework.stereotype.Component;

@Component
public class Android implements OperatingSystem {

    @Override
    public String specification(){

        return "Android : The most popular OS ";

    }

}
