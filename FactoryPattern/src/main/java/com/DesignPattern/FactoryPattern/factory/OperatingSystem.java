package com.DesignPattern.FactoryPattern.factory;

public interface OperatingSystem {

    public String specification();

}
