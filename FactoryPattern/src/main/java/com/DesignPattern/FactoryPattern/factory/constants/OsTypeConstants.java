package com.DesignPattern.FactoryPattern.factory.constants;

public class OsTypeConstants {


    public static final String  ANDROID = "google-type";
    public static final String  IOS = "apple-type";
    public static final String  WINDOWS = "microsoft-type";

}


