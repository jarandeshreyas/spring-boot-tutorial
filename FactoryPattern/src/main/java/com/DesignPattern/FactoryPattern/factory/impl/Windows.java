package com.DesignPattern.FactoryPattern.factory.impl;

import com.DesignPattern.FactoryPattern.factory.OperatingSystem;
import org.springframework.stereotype.Component;

@Component
public class Windows implements OperatingSystem {

    @Override
    public String specification(){

        return "Windows : Best OS for PC ";

    }

}
