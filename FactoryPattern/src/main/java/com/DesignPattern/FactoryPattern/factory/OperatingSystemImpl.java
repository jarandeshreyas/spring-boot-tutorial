package com.DesignPattern.FactoryPattern.factory;

import com.DesignPattern.FactoryPattern.factory.constants.OsTypeConstants;
import com.DesignPattern.FactoryPattern.factory.impl.Android;
import com.DesignPattern.FactoryPattern.factory.impl.Ios;
import com.DesignPattern.FactoryPattern.factory.impl.Windows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class OperatingSystemImpl {

    @Autowired
    Android android;

    @Autowired
    Ios ios;

    @Autowired
    Windows windows;

    private static final Map<String, OperatingSystem> handler = new HashMap<String, OperatingSystem>();

    @PostConstruct
    private Map<String, OperatingSystem> getObject() {

        handler.put(OsTypeConstants.ANDROID, android);
        handler.put(OsTypeConstants.IOS, ios);
        handler.put(OsTypeConstants.WINDOWS, windows);

        return handler;
    }

    public static OperatingSystem createInstance(String os) throws Exception {

        return Optional.ofNullable(handler.get(os)).orElseThrow(() -> new IllegalArgumentException("Invalid Operating System"));

    }



}
