package com.DesignPattern.FactoryPattern.controller;

import com.DesignPattern.FactoryPattern.factory.OperatingSystem;
import com.DesignPattern.FactoryPattern.factory.OperatingSystemImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FactoryController {

    @GetMapping("/OperatingSystem/{os}")
    public String getOsTypeInfo(@PathVariable("os") String os) throws Exception{

        OperatingSystem osFactory= OperatingSystemImpl.createInstance(os);
        return osFactory.specification();

    }

}
