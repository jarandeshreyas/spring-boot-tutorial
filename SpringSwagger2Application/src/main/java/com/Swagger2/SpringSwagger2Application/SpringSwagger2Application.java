package com.Swagger2.SpringSwagger2Application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@SpringBootApplication
@EnableSwagger2
public class SpringSwagger2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringSwagger2Application.class, args);
	}

	@Bean
	public Docket docket(){

		return new Docket(DocumentationType.SWAGGER_2).select().paths(PathSelectors.ant("/data/*"))
				.apis(RequestHandlerSelectors.basePackage("com.Swagger2.SpringSwagger2Application"))
				.build()
				.apiInfo(apiDetails());
	}

	private ApiInfo apiDetails(){

		return new ApiInfo(
				"Address book API",
				"Sample API for Swagger tutorial",
				"1.0",
				"Free to use",
				new springfox.documentation.service.Contact("Shreyas Jarande", "shreyas.com", "shreyasjarande04@gmail.com"),
				"API License",
				"shreyas04.com",
				Collections.emptyList());



	}

}
