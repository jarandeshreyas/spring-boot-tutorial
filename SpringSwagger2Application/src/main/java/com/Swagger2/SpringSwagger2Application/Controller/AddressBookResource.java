package com.Swagger2.SpringSwagger2Application.Controller;

import com.Swagger2.SpringSwagger2Application.Model.Contact;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@RestController
@RequestMapping("/data")
public class AddressBookResource {

    ConcurrentMap<String, Contact> contacts=new ConcurrentHashMap<>();

    @GetMapping("/{id}")
    @ApiOperation( value = "find contacts by id",
                    notes = "Provide an id to look in ids which are registered",
                    response = Contact.class)
    public Contact getContact(@ApiParam(value = "id value for contact yiu need to retrieve", required = true) @PathVariable("id") String id){

        return contacts.get(id);

    }

    @GetMapping("/getall")
    public List<Contact> getAllContacts(){

        return new ArrayList<Contact>(contacts.values());

    }

    @PostMapping("/post")
    public Contact addContact(@RequestBody Contact contact){

        contacts.put(contact.getId(), contact);
        return contact;

    }

}
