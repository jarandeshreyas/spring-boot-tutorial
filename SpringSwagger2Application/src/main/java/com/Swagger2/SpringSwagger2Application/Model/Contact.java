package com.Swagger2.SpringSwagger2Application.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "details about Contact class")
public class Contact {

    @ApiModelProperty(notes = "the id Param which should be unique")
    private String id;
    @ApiModelProperty(notes = "the name param which indicates the person's name")
    private String name;
    @ApiModelProperty(notes = "the phone param which indicates the person's phone number")
    private String phone;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
