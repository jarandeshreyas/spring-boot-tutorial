package com.DesignPattern.SingletonPattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class config {

    @Autowired
    private Student stud1;

    @Autowired
    private Student stud2;

    @Bean
    void dispAge(){

        stud1.setAge(21);
        System.out.println("Age for student 1 after setting student 1 is "+stud1.getAge());
        System.out.println("Age for student 2 after setting student 1 is "+stud2.getAge());

        stud2.setAge(7);
        System.out.println("Age for student 1 after setting student 2 is "+stud1.getAge());
        System.out.println("Age for student 2 after setting student 2 is "+stud2.getAge());

        System.out.println("Hash code for stud1 is "+stud1.hashCode());
        System.out.println("Hash code for stud2 is "+stud2.hashCode());

    }

}
