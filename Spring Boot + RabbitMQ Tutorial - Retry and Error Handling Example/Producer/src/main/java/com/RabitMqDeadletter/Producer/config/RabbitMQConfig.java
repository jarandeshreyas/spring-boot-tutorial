package com.RabitMqDeadletter.Producer.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;

@Configuration
public class RabbitMQConfig {

    @Bean
    DirectExchange deadLetterExchange(){
        return new DirectExchange("deadLetterExchange ");
    }

    @Bean
    Queue dlq(){
        return QueueBuilder.durable("deadLetter.queue").build();
    }

    @Bean
    Queue queue(){
        return QueueBuilder.durable("javainuse.queue").withArgument("x-dead-letter-exchange", "deadLetterExchange").withArgument("x-dead-letter-routing-key", "deadLetter").build();
    }


    @Bean
    DirectExchange exchange() {
        return new DirectExchange("javainuseExchange");
    }


    @Bean
    Binding DLQbinding (Queue dlq, DirectExchange deadLetterExchange){
            return BindingBuilder.bind(dlq).to(deadLetterExchange).with("deadLetter");
    }

    @Bean
    Binding binding(Queue queue, DirectExchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with("javainuse");
    }

    @Bean
    public MessageConverter jsonMessageConverter(){
        return new Jackson2JsonMessageConverter();
    }


    public AmqpTemplate rabbitTemplate(ConnectionFactory connectionFactory){
        final RabbitTemplate rabbitTemplate=new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }

}
