package com.RabitMqDeadletter.Producer.controller;

import com.RabitMqDeadletter.Producer.model.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/javainuserabbitmq")
public class RabbitMQWebController {

    @Autowired
    private AmqpTemplate amqpTemplate;

    Logger log=LoggerFactory.getLogger(RabbitMQWebController.class);

    @GetMapping("/producer/{empName}/{empId}/{salary}")
    public String Producer(@PathVariable("empName") String empName, @PathVariable("empId") String empId, @PathVariable("salary") int salary){

        Employee emp=new Employee();
        emp.setEmpId(empId);
        emp.setEmpName(empName);
        emp.setSalary(salary);

        log.info("Converting and sending message ");
        amqpTemplate.convertAndSend("javainuseExchange", "javainuse", emp);

        return "Message sent to rabbitmq successfully ";

    }

}
