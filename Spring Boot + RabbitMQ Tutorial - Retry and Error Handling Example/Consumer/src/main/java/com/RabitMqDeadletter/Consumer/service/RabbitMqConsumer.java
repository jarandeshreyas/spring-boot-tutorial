package com.RabitMqDeadletter.Consumer.service;

import com.RabitMqDeadletter.Consumer.exception.InvalidSalaryException;
import com.RabitMqDeadletter.Consumer.model.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class RabbitMqConsumer {

    private static final Logger logger= LoggerFactory.getLogger(RabbitMqConsumer.class);

    @RabbitListener(queues = "javainuse.queue")
    public void receivedMessage(Employee employee) throws InvalidSalaryException {

        logger.info("Received Message from RabbitMQ : " + employee);
        if (employee.getSalary()<0){
            throw new InvalidSalaryException();
        }
    }

}
