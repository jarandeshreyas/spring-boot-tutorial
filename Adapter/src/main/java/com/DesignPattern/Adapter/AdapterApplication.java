package com.DesignPattern.Adapter;

import org.junit.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdapterApplication {

	@Test
	public static void main(String[] args) throws Exception {
		SpringApplication.run(AdapterApplication.class, args);

		String testString=" Formatting line 1. Formatting line 2. Formatting line 3.";
		TextFormattable newLineFormatter=new TextFormattableImpl();
		String resultString=newLineFormatter.formatText(testString);
		System.out.println("Result String is \n"+resultString);

		CsvFormattable csvFormattable=new CsvFormattableImpl();
		TextFormattable csvAdapter=new CsvAdapter(csvFormattable);
		String resultCsvString=csvAdapter.formatText(testString);
		System.out.println("Result CSV String is \n "+resultCsvString);

	}

}
