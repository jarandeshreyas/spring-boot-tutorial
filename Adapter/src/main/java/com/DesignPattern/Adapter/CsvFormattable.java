package com.DesignPattern.Adapter;

public interface CsvFormattable {

     String formatCsvText(String text);

}
