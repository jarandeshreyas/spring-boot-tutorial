package com.DesignPattern.Adapter;

public class TextFormattableImpl implements TextFormattable {

    public String formatText(String text){
        String formattedText=text.replace(".","\n");
        return formattedText;

    }

}
