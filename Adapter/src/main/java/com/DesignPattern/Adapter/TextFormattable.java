package com.DesignPattern.Adapter;

public interface TextFormattable {

    String formatText(String text);

}
