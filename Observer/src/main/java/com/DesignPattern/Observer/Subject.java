package com.DesignPattern.Observer;

import java.math.BigDecimal;

public interface Subject {

    public void registeredObserver(Observer observer);
    public void notifyObservers();
    public void removeObserver(Observer observer);
    public void setBidAmount(Observer observer, BigDecimal newBidAmount);

}
