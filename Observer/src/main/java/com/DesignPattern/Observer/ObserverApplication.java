package com.DesignPattern.Observer;

import org.junit.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;

@SpringBootApplication
public class ObserverApplication {

	@Test
	public static void main(String[] args) throws Exception {
		SpringApplication.run(ObserverApplication.class, args);

		Subject product=new Product("36 inch LED TV",new BigDecimal(350));
		Observer bidder1=new Bidder("Alex Parker");
		Observer bidder2=new Bidder("Henry Smith");
		Observer bidder3=new Bidder("Mary Peterson");
		product.registeredObserver(bidder1);
		product.registeredObserver(bidder2);
		product.registeredObserver(bidder3);
		product.setBidAmount(bidder1, new BigDecimal(375));
		product.removeObserver(bidder2);
		product.setBidAmount(bidder3, new BigDecimal(400));

	}

}
