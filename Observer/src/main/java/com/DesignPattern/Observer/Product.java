package com.DesignPattern.Observer;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Product implements Subject {


    private final ArrayList<Observer> observers = new ArrayList<>();
    private final String productName;
    private BigDecimal bidAmount;
    private Observer observer;

    public Product(String productName, BigDecimal bidAmount) {
        this.productName = productName;
        this.bidAmount = bidAmount;
    }

    @Override
    public void registeredObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void notifyObservers() {
        System.out.println("-----------------New bid placed----------------");
        for (Observer ob : observers) {
            ob.update(this.observer,this.productName,this.bidAmount );
        }
    }

    @Override
    public void removeObserver(Observer observer) {

        observers.remove(observer);
        System.out.println("-----------------"+observer+" has withdrawn from bidding----------------");

    }

    @Override
    public void setBidAmount(Observer observer, BigDecimal newBidAmount) {

        int res = bidAmount.compareTo(newBidAmount);
        if(res==-1){
            this.observer=observer;
            this.bidAmount=newBidAmount;
            notifyObservers();
        }
        else{
            System.out.println("new bid amount cannot be less than the current bid " +
                    "amount or cannot be equal to the current bid amount ");
        }

    }
}
