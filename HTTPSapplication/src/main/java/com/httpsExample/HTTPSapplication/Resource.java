package com.httpsExample.HTTPSapplication;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/root")
public class Resource {

    @GetMapping
    public String printString(){
        return "Welcome to the world of https ";
    }

}
