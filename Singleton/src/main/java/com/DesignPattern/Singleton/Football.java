package com.DesignPattern.Singleton;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("sport")
@Scope("singleton")
public class Football {

    private String name;

    public Football(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
