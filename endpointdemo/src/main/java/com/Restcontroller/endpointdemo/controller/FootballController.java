package com.Restcontroller.endpointdemo.controller;


import com.Restcontroller.endpointdemo.Sports.Football;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/football")
public class FootballController {

    @RequestMapping("/playerdetails")
    public Football football(){
        return new Football(30, "LionelMessi", "ParisSaintGerman");
    }

    @RequestMapping("/customdetails/{param2}/{param1}")
    public Football fb(@PathVariable("param1") String teamName, @PathVariable("param2") String playerName){
        return new Football(7,playerName,teamName);
    }

}
