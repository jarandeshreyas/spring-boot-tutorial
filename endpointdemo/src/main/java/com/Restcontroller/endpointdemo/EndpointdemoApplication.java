package com.Restcontroller.endpointdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EndpointdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EndpointdemoApplication.class, args);
	}

}
