package com.Restcontroller.endpointdemo.Sports;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
@Getter
public class Football {

    private int jerseyNo;
    private String playerName;
    private String teamName;

}
