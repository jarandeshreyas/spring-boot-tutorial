package com.PostgreSQL.SpringPostGreSQL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class SpringPostGreSqlApplication implements CommandLineRunner {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public static void main(String[] args) {
		SpringApplication.run(SpringPostGreSqlApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		String sql="INSERT INTO students VALUES('17', 'ronaldo77@gmail.com', 'Christiano Ronaldo')";

		int rows=jdbcTemplate.update(sql);

		if(rows>0){
			System.out.println("new row has been inserted ");
		}

	}
}
